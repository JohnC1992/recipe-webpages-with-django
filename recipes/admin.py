from django.contrib import admin
from recipes.models import Recipe, RecipeStep, RecipeIngredient

@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "id",
    )

@admin.register(RecipeStep)
class RecipeStepsAdmin(admin.ModelAdmin):
    list_display=(
        "step_number",
        "id",
        "instruction",
    )

@admin.register(RecipeIngredient)
class RecipeIngredientAdmin(admin.ModelAdmin):
    list_display=(
        "id",
        "amount",
        "food_item",
    )
