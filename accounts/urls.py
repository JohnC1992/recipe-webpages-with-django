from django.urls import path
from accounts.views import signup, user_login, user_signout

urlpatterns = [
    path("signup/",signup, name="signup"),
    path("login/",user_login, name="login"),
    path("",user_signout, name='signout'),
]
